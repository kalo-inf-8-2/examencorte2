package com.example.examencorte2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import database.Usuario;
import database.UsuariosManager;

public class MainActivity extends AppCompatActivity {

    private EditText edtUser;
    private EditText edtPassword;
    private Button btnIngresar;
    private Button btnRegistrarme;
    private UsuariosManager db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtUser = (EditText) findViewById(R.id.edtUser);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnRegistrarme = (Button) findViewById(R.id.btnRegistrarme);
        db = new UsuariosManager(MainActivity.this);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtUser.getText().toString().equals("") || edtPassword.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Es necesario llenar los dos campos para ingresar", Toast.LENGTH_SHORT).show();
                } else {
                    db.openDatabase();
                    Usuario user = db.getUsuario(edtUser.getText().toString(), edtPassword.getText().toString());
                    if(user == null) {
                        Toast.makeText(MainActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(MainActivity.this, IngresoActivity.class);
                        intent.putExtra("nombre", user.getName());
                        startActivityForResult(intent, 0);
                    }
                    db.cerrar();
                }
            }
        });

        btnRegistrarme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegistroActivity.class);
                startActivityForResult(intent, 0);
            }
        });

    }
}
