package com.example.examencorte2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import database.Usuario;
import database.UsuariosManager;

public class RegistroActivity extends AppCompatActivity {

    private EditText edtUsuario;
    private EditText edtPass1;
    private EditText edtPass2;
    private EditText edtNombre;
    private Button btnRegistro;
    private Button btnLimpiar;
    private UsuariosManager db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        edtUsuario = (EditText) findViewById(R.id.edtUsuario);
        edtPass1 = (EditText) findViewById(R.id.edtPass1);
        edtPass2 = (EditText) findViewById(R.id.edtPass2);
        edtNombre = (EditText) findViewById(R.id.edtNombre);
        btnRegistro = (Button) findViewById(R.id.btnRegistro);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        db = new UsuariosManager(RegistroActivity.this);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtUsuario.getText().toString().equals("") || edtPass1.getText().toString().equals("") || edtPass2.getText().toString().equals("") || edtNombre.getText().toString().equals("")) {
                    Toast.makeText(RegistroActivity.this, "Es necesario llenar todos los campos", Toast.LENGTH_SHORT).show();
                } else {
                    if(edtPass1.getText().toString().equals(edtPass2.getText().toString())) {
                        Usuario user = new Usuario();
                        user.setUser(edtUsuario.getText().toString());
                        user.setPassword(edtPass1.getText().toString());
                        user.setName(edtNombre.getText().toString());
                        db.openDatabase();
                        long idx = db.insertarUsuario(user);
                        Toast.makeText(RegistroActivity.this, "Se interso un usuario con ID = " + idx, Toast.LENGTH_SHORT).show();
                        db.cerrar();
                        Intent intent = new Intent(RegistroActivity.this, MainActivity.class);
                        startActivityForResult(intent, 0);
                    } else {
                        Toast.makeText(RegistroActivity.this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edtUsuario.setText("");
                edtPass1.setText("");
                edtPass2.setText("");
                edtNombre.setText("");
            }
        });
    }
}
