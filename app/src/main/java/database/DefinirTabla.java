package database;

import android.provider.BaseColumns;

public class DefinirTabla {
    public DefinirTabla(){}
    public static abstract class Usuario implements BaseColumns {
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_USER = "user";
        public static final String COLUMN_NAME_PASSWORD = "password";
        public static final String COLUMN_NAME_NAME = "name";
    }
}
