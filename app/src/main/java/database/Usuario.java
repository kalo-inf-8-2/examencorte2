package database;

import java.io.Serializable;

public class Usuario implements Serializable {
    private int _ID;
    private String user;
    private String password;
    private String name;

    public Usuario() {
    }

    public Usuario(int _ID, String user, String password, String name) {
        this._ID = _ID;
        this.user = user;
        this.password = password;
        this.name = name;
    }

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}