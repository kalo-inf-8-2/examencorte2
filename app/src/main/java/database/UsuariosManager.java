package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UsuariosManager {
    private Context context;
    private UsuarioDbHelper usuarioDbHelper;
    private SQLiteDatabase db;
    private String[] columnToRead = new String[] {
            DefinirTabla.Usuario._ID, DefinirTabla.Usuario.COLUMN_NAME_USER, DefinirTabla.Usuario.COLUMN_NAME_PASSWORD, DefinirTabla.Usuario.COLUMN_NAME_NAME
    };

    public UsuariosManager(Context context) {
        this.context = context;
        this.usuarioDbHelper = new UsuarioDbHelper(this.context);
    }

    public void openDatabase() {
        db = usuarioDbHelper.getWritableDatabase();
    }

    public long insertarUsuario(Usuario user) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Usuario.COLUMN_NAME_USER, user.getUser());
        values.put(DefinirTabla.Usuario.COLUMN_NAME_PASSWORD, user.getPassword());
        values.put(DefinirTabla.Usuario.COLUMN_NAME_NAME, user.getName());

        return db.insert(DefinirTabla.Usuario.TABLE_NAME, null, values);
    }

    public Usuario leerUsuario(Cursor cursor) {
        Usuario user = new Usuario();

        user.set_ID(cursor.getInt(0));
        user.setUser(cursor.getString(1));
        user.setPassword(cursor.getString(2));
        user.setName(cursor.getString(3));

        return user;
    }

    public Usuario getUsuario(String user, String password) {
        Usuario usuario = null;

        SQLiteDatabase db = this.usuarioDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Usuario.TABLE_NAME, columnToRead, DefinirTabla.Usuario.COLUMN_NAME_USER + " = ? AND " + DefinirTabla.Usuario.COLUMN_NAME_PASSWORD + " = ? ", new String[] {String.valueOf(user), String.valueOf(password)}, null,null,null);
        if(c.moveToFirst()) {
            usuario = leerUsuario(c);
        }
        c.close();
        return usuario;
    }

    public void cerrar() {
        usuarioDbHelper.close();
    }
}